;;; r.el --- r language config
;;; Commentary:
;;; Code:

;; load packages
(dolist (package '(ess-smart-underscore))
    (unless (package-installed-p package)
    (package-install package)))
(require 'ess)
(load "ess-autoloads")
(require 'ess-smart-underscore)
(setq ess-S-assign' " <- ")
(add-hook 'ess-mode-hook (lambda ()
                           (setq ess-S-underscore-when-last-character-is-a-space t)
                           (fci-mode)))

;; (add-hook 'ess-mode-hook
;;           '(lambda () (set (make-local-variable 'indent-region-function)
;;                            'ess-indent-region-as-R-function)))
