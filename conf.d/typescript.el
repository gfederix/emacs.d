;;; typescript.el --- Typescript config file
;;; Commentary:
;;; Code:

;; Tpescript
;; (require 'js-doc)
;; (setq js-doc-mail-address "federix@ya.ru"
;;        js-doc-author (format "Fedor Goncharov <%s>" js-doc-mail-address)
;;        js-doc-url "none"
;;        js-doc-license "AGPL")
(dolist (package '(tide))
  (unless (package-installed-p package)
    (package-install package)))
(add-hook 'typescript-mode-hook
          (lambda ()
            (tide-setup)
            (flycheck-mode +1)
            (setq flycheck-check-syntax-automatically '(save mode-enabled))
            (eldoc-mode +1)
            ;; (define-key typescript-mode-map "\C-ci" 'js-doc-insert-function-doc)
            ;; (define-key typescript-mode-map "@" 'js-doc-insert-tag)
            ;; company is an optional dependency. You have to
            ;; install it separately via package-install
            (company-mode-on)))
