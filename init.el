;;; init.el --- Startup config file
;;; Commentary:
;; based on https://github.com/magnars/.emacs.d/blob/master/init.el

;;; Code:

;; Turn off mouse interface early in startup to avoid momentary display
(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
;; No splash screen, please.
(setq inhibit-startup-message t)
;; Disable Dialog Boxes.
(setq use-dialog-box '())
;; Font & Geometry
(dolist (opt '((font . "Inconsolata-13") (line-spacing . 0) (width . 84) ))
  (add-to-list 'default-frame-alist opt))
;; (if                                     ; for large and wide descktop
;;     (= (display-pixel-width) 1920)
;;     (add-to-list 'default-frame-alist '(font . "Inconsolata-10")))

;; (dolist (opt '((font . "Inconsolata-10") (line-spacing . 0) (width . 84) ))
;;   (add-to-list 'default-frame-alist opt))

;; Save point position between sessions
(require 'saveplace)
(setq-default save-place t)
(setq save-place-file (expand-file-name ".places" user-emacs-directory))
;; Set path to dependencies
(setq site-lisp-dir
      (expand-file-name "site-lisp" user-emacs-directory))
(setq settings-dir
      (expand-file-name "settings" user-emacs-directory))
;; Set up load path
(add-to-list 'load-path settings-dir)
(add-to-list 'load-path site-lisp-dir)

;; Keep emacs Custom-settings in separate file
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
;; create empy file if it not exist
(unless (file-exists-p custom-file)
  (write-region "" nil custom-file))
(load custom-file)

;; Packages
(require 'package)
;; Repositary
;;(dolist (repo '(
;;                ("melpa" . "http://stable.melpa.org/packages/"))
;;                ;;("melpa" . "http://melpa.org/packages/"))
;;               )
;;  (add-to-list 'package-archives repo t))
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
  ;;(add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  (add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
  (when (< emacs-major-version 24)
    ;; For important compatibility libraries like cl-lib
    (add-to-list 'package-archives '("gnu" . (concat proto "://elpa.gnu.org/packages/")))))
;; Pin package to the specific dirrectory
(setq package-pinned-packages '())
;; Activate all the packages (in particular autoloads)
(if (version< emacs-version "27.0")
    (package-initialize))
;; Fetch the list of packages available
(unless package-archive-contents
  (package-refresh-contents))
;; Install the missing packages
(dolist (package '( flycheck
                    color-theme-modern
                    company
                    company-quickhelp
                    magit))
  (unless (package-installed-p package)
    (package-install package)))

(load-theme 'dark-laptop)

;; Set up appearance early
;; (require 'appearance)

;; Write backup files to own directory
(setq backup-directory-alist
      `(("." . ,(expand-file-name
                 (concat user-emacs-directory "backups")))))

;; fci-mode
(require 'fill-column-indicator)
(define-globalized-minor-mode global-fci-mode fci-mode (lambda () (fci-mode 1)))
(global-fci-mode 1)
(setq fci-rule-width 1)
(setq fci-rule-color "darkblue")
(setq-default fill-column 80)
(defun auto-fci-mode (&optional unused)
  (if (> (window-width) fill-column)
      (fci-mode 1)
    (fci-mode 0))
  )
(add-hook 'after-change-major-mode-hook 'auto-fci-mode)
(add-hook 'window-configuration-change-hook 'auto-fci-mode)

;; aligns annotation to the right hand side
(setq company-tooltip-align-annotations t)
(company-quickhelp-mode)
;; (global-set-key "\C-\M-i" 'company-complete)
(eval-after-load 'company
  '(progn
     (define-key company-mode-map (kbd "C-:") 'helm-company)
     (define-key company-active-map (kbd "C-:") 'helm-company)
     (define-key company-active-map (kbd "C-M-i") 'company-complete-common)
     (define-key company-active-map (kbd "M-n") nil)
     (define-key company-active-map (kbd "M-p") nil)
     (define-key company-active-map (kbd "C-n") #'company-select-next)
     (define-key company-active-map (kbd "C-p") #'company-select-previous)
     )
  )

;; Badge code
;; FlyCheck mode
(add-hook 'after-init-hook #'global-flycheck-mode)
;; Speed up flycheck
(setq eldoc-idle-delay 1) ;; in seconds
(setq flycheck-highlighting-mode "lines")
;; Windmove: SHIFT-ARROW to move anouther window
(when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings))

;; Emacs server
(require 'server)
(unless (server-running-p)
  (server-start))
;; Load other Configs
(require 'load-directory)
(load-directory
 (expand-file-name "conf.d" user-emacs-directory))
(setq geiser-active-implementations '(mit racket guile))
