;;; ag.el --- Golden search packge
;;; Commentary:
;;; Code:

(dolist (package 'ag)
    (unless (package-installed-p package)
      (package-install package)))

(require 'ag)
