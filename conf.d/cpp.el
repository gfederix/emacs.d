;;; cpp.el --- C++ mode configuration
;;; Commentary:
;;; Code:
(add-hook 'c++-mode-hook
          (lambda ()
            (setq flycheck-clang-language-standard "c++14"
                  )))
