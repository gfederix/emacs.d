;;; web-mode.el --- Web mode
;;; Commentary:
;;; Code:
;; Assosiate mode with file names
(dolist (package '(web-mode coffee-mode pug-mode sass-mode flymake-sass))
    (unless (package-installed-p package)
      (package-install package)))
(let ((web-mode-files
       '("\\.html?\\'" "\\.phtml\\'" "\\.tpl\\.php\\'" "\\.[agj]sp\\'"
         "\\.as[cp]x\\'" "\\.erb\\'" "\\.mustache\\'" "\\.djhtml\\'")))
  (require 'web-mode)
  (dolist (file web-mode-files)
    (add-to-list 'auto-mode-alist `(,file . web-mode))))

