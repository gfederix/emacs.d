;;; python.el --- python config
;;; Commentary:
;;; Code:

(dolist (package '(elpy))
    (unless (package-installed-p package)
    (package-install package)))
(elpy-enable)
(setq elpy-rpc-python-command "python3")
(setq python-shell-interpreter "jupyter"
      python-shell-interpreter-args "console --simple-prompt"
      python-shell-prompt-detect-failure-warning nil)
(add-to-list 'python-shell-completion-native-disabled-interpreters
             "jupyter")
(add-hook 'elpy-mode-hook
          (lambda ()
            (highlight-indentation-mode -1)
            ;; (toggle-truncate-lines '())
            ))
