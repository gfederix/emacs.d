(dolist (package '(magit))
  (unless (package-installed-p package)
    (package-install package)))
(setq vc-handled-backends '(Git Hg SVN CVS RCS SCCS Bzr Arch))

(global-set-key (kbd "C-x g") 'magit-status)
(global-set-key (kbd "C-x M-g") 'magit-dispatch-popup)
