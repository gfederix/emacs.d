;;; globals.el --- global configs
;;; Commentary:
;;; Code:

(setq-default indent-tabs-mode nil)
(add-hook 'after-init-hook 'global-company-mode)
;; Global auto refresh file mode
(global-auto-revert-mode 1)
(column-number-mode t)
(global-linum-mode t)
(show-paren-mode t)
(setq fci-rule-width 1)
(setq fill-column 80)
(savehist-mode t)
